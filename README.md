# haXlab Toolkit - Ecobee Thermostat

Please visit https://haxlab.atlassian.net/wiki/spaces/ET/overview for more up to date documentation.

The docs folder contains all the documents for this fixture

The scripts folder contains the scripts found in ~/haXlab-ecobee

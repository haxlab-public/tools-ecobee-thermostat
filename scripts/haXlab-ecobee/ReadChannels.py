import serial
import time

ser = serial.Serial('/dev/canakit',115200, timeout=1)
ser.write('\n\r')
val = ser.read(size=64)
#print (val)
ser.write('CHS.GET\n\r')
cmdLen = len('CHS.GET\n\r')
dataLen = len('0 0 0 0 0 0\n\r')
echo = ser.read(size=cmdLen)
val = ser.read(size=dataLen)
#print (val)

val = val.rstrip()
dataSplit = val.split(' ')
#print (dataSplit)


if dataSplit[0] == '1':
    print ("Cooling On")
else:
    print ("Cooling Off")
if dataSplit[1] == '1':
    print ("Heating On")
else:
    print ("Heating Off")
if dataSplit[2] == '1':
    print ("Fan On")
else:
    print ("Fan Off")

ser.close()

